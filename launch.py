import configparser
import logging
import logging.config
import re
import sys
import time
from datetime import datetime, timedelta
from html.parser import HTMLParser

import praw
import prawcore
import prawcore.exceptions
import urllib3
from layer7_utilities import oAuth, LoggerConfig
from requests.exceptions import Timeout
from _version import __version__

botconfig = configparser.ConfigParser()
botconfig.read("botconfig.ini")

__botname__ = "Countdown"
__description__ = "Updates sidebar with time remaining until an event or daily/weekly reset"
__author__ = "/u/D0cR3d"
__dsn__ = botconfig.get("BotConfig", "DSN")
__logpath__ = botconfig.get("BotConfig", "logpath")

SUBREDDIT = "DestinyTheGame"
widgetid = "widget_15obkhjgd5ae9"
specialtarget = ""  # UTC, empty for none
weeklyreset = "Tuesday"  # day in english, time will be like daily reset
dailyreset = 17  # hour in UTC

SleepTime = 60 * 10  # Time to sleep between rounds, in seconds

newRedditText = """**Weekly Reset** in {ctdwn_1_days} days, {ctdwn_1_hours} hours and {ctdwn_1_minutes} minutes

**Daily Reset** in {ctdwn_2_hours} hours and {ctdwn_2_minutes} minutes
"""

"""
To display the special Countdown, insert the following AFTER "[](https://destinyreddit.com/flair)" in the sidebar:

### Warmind DLC Release in:

### [-2](#days) Days, [9](#hours) Hours and [39](#minutes) Minutes
"""


class CountdownBot(object):
    def __init__(self):
        self.r = None

        # Create the logger
        loggerconfig = LoggerConfig(__dsn__, __botname__, __version__, __logpath__)
        logging.config.dictConfig(loggerconfig.get_config())
        self.log = logging.getLogger("root")
        self.log.info(u"/*********Starting App*********\\")
        self.log.info(u"App Name: {} | Version: {}".format(
            __botname__, __version__))

        self.__init_configs()
        self.login()
        self.subreddit = self.r.subreddit(SUBREDDIT)

        if specialtarget != "":
            self.specialtarget_datetime = datetime.strptime(
                specialtarget, "%B %d %Y %H:%M:%S"
            )
        else:
            self.specialtarget_datetime = None
        self.weeklyreset_day = weeklyreset
        self.dailyreset_hour = dailyreset

        widgets = self.subreddit.widgets
        self.widget = None
        for widget in widgets.sidebar:
            if widget.id == widgetid:
                self.widget = widget
                break
        if self.widget is None:
            self.log.error(f"no widget found with id {widgetid}")

    def __init_configs(self):
        self.DB_USERNAME = botconfig.get("Database", "Username")
        self.DB_PASSWORD = botconfig.get("Database", "Password")
        self.DB_HOST = botconfig.get("Database", "Host")
        self.DB_NAME = botconfig.get("Database", "DatabaseName")

    def login(self):
        auth = oAuth()
        auth.get_accounts(
            "dtgbot",
            __description__,
            __version__,
            __author__,
            __botname__,
            self.DB_USERNAME,
            self.DB_PASSWORD,
            self.DB_HOST,
            self.DB_NAME,
        )

        for account in auth.accounts:
            self.r = account.login()
        self.log.info("Connected to account: {}".format(self.r.user.me()))

    def compute_time_ago_params(self, target, prefix=""):
        countdown_delta = target - datetime.utcnow()
        days = countdown_delta.days
        hours = countdown_delta.seconds // 3600
        minutes = (countdown_delta.seconds - (hours * 3600)) // 60
        seconds = countdown_delta.seconds - (hours * 3600) - (minutes * 60)

        return {
            prefix + "days": days,
            prefix + "hours": hours,
            prefix + "minutes": minutes,
            prefix + "seconds": seconds,
        }

    def update_countdown(self):
        settings = self.subreddit.mod.settings()
        old_description = HTMLParser().unescape(settings["description"])
        new_description = HTMLParser().unescape(settings["description"])
        widget_desc = newRedditText

        now = datetime.utcnow()
        dailyreset = now.replace(hour=self.dailyreset_hour, minute=0, second=0)
        if now.hour >= self.dailyreset_hour:
            dailyreset += timedelta(1)
        weeklyreset = dailyreset
        while weeklyreset.strftime("%A") != self.weeklyreset_day:
            weeklyreset += timedelta(1)

        toupdate = {}
        if self.specialtarget_datetime is not None:
            new = self.compute_time_ago_params(self.specialtarget_datetime)
            toupdate = {**toupdate, **new}
        weekly = self.compute_time_ago_params(weeklyreset, "ctdwn_1_")
        daily = self.compute_time_ago_params(dailyreset, "ctdwn_2_")
        toupdate = {**toupdate, **weekly, **daily}

        for key, value in toupdate.items():
            # replace [<anything>](#<key>)
            pattern = "\\[[^\\]]*\\]\\(#{0}\\)".format(key)
            repl = "[{0}](#{1})".format(value, key)  # with [<value>](#<key>)
            new_description = re.sub(pattern, repl, new_description)

            widget_desc = widget_desc.replace("{"+str(key)+"}", str(value))

        if old_description != new_description:
            self.subreddit.mod.update(description=new_description)
            self.log.info("Updated settings with new Countdown")
        else:
            self.log.debug("old sidebar and new sidebar are same. Skipping update.")
        if self.widget is not None:
            old_text = self.widget.text
            if old_text != widget_desc:
                self.widget.mod.update(text=widget_desc)
                self.log.info('Updated redesign sidebar with new text')
            else:
                self.log.debug('Redesign old sidebar and new sidebar are same. Skipping update.')

    def main(self):
        try:
            self.update_countdown()

        except UnicodeEncodeError as e:
            self.log.error("Caught UnicodeEncodeError! {}".format(str(e)))
        except prawcore.exceptions.InvalidToken:
            self.log.warning(
                "API Token Error. Likely on reddits end. Issue self-resolves."
            )
        except prawcore.exceptions.BadJSON:
            self.log.warning(
                "PRAW didn't get good JSON, probably reddit sending bad data due to site issues."
            )
        except (
            prawcore.exceptions.ResponseException,
            prawcore.exceptions.RequestException,
            prawcore.exceptions.ServerError,
            urllib3.exceptions.TimeoutError,
            Timeout,
        ):
            self.log.warning(
                "HTTP Requests Error. Likely on reddits end due to site issues."
            )
            time.sleep(300)
        except praw.exceptions.APIException:
            self.log.error("PRAW/Reddit API Error")
            time.sleep(30)
        except praw.exceptions.ClientException:
            self.log.error("PRAW Client Error")
            time.sleep(30)
        except KeyboardInterrupt as e:
            self.log.warning("Caught KeyboardInterrupt - Exiting")
            sys.exit()
        except Exception:
            self.log.critical("General Exception - Sleeping 5 min")
            time.sleep(300)


def main():
    bot = CountdownBot()

    while bot:
        bot.main()
        time.sleep(SleepTime)
